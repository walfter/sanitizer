<?php

namespace Walfter\Sanitizer;

use Walfter\Sanitizer\Abstracts\AbstractValidator;
use Walfter\Sanitizer\Contracts\ValidatorInterface;

class Validator extends AbstractValidator implements ValidatorInterface
{
    // Default validator
}