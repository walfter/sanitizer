<?php

namespace Walfter\Sanitizer;

use Walfter\Sanitizer\Contracts\SanitizerInterface;
use Walfter\Sanitizer\Contracts\ValidatorInterface;
use Walfter\Sanitizer\Exceptions\BadValidatorException;

class Sanitizer implements SanitizerInterface
{
    protected static string $validatorDefaultClass = Validator::class;

    /**
     * @inheritDoc
     */
    public static function validated(array $rules, array $data, array $messages = []): array
    {
        self::checkValidator(self::$validatorDefaultClass);

        /** @var ValidatorInterface $validator */
        $validator = new self::$validatorDefaultClass($rules, $data, $messages);

        return $validator->validated();
    }

    private static function checkValidator(string $validatorClass): void
    {
        if (!class_implements($validatorClass, ValidatorInterface::class)) {
            throw new BadValidatorException();
        }
    }
}