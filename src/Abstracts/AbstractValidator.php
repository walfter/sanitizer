<?php

namespace Walfter\Sanitizer\Abstracts;

use Walfter\Sanitizer\Contracts\BadRuleExceptionInterface;
use Walfter\Sanitizer\Contracts\RuleExceptionInterface;
use Walfter\Sanitizer\Contracts\RuleInterface;
use Walfter\Sanitizer\Contracts\ValidatorInterface;
use Walfter\Sanitizer\Exceptions\BadRuleException;
use Walfter\Sanitizer\Exceptions\RuleException;

abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var array<string, RuleInterface[]>
     */
    protected array $rules;

    /**
     * @var array<string, mixed>
     */
    protected array $data = [];

    /**
     * @var array<string, mixed>
     */
    protected array $validatedData = [];

    /**
     * @var array<string, string>
     */
    protected array $messages = [];

    /**
     * @inheritDoc
     */
    public function __construct(array $rules, array $data = [], array $messages = [])
    {
        $this->setRules($rules);

        $this->data = $data;
        $this->messages = $messages;
    }

    public function setData(array $data): ValidatorInterface
    {
        $this->data = $data;

        return $this;
    }

    public function setMessages(array $messages): ValidatorInterface
    {
        $this->messages = $messages;

        return $this;
    }


    /**
     * @param array $rules
     * @return void
     *
     * @throws BadRuleExceptionInterface
     */
    protected function setRules(array $rules): void
    {
        $this->checkRules($rules);
        $this->rules = $this->prepareRules($rules);
    }

    /**
     * @param array $rules
     *
     * @return void
     *
     * @throws BadRuleException
     */
    protected function checkRules(array $rules): void
    {
        foreach ($rules as $field => $fieldRules) {
            foreach ($fieldRules as $rule) {
                if ($this->isRule($rule)) {
                    continue;
                }

                $className = is_string($rule) ? $rule : get_class($rule);
                $message = sprintf('Key %s, rule %s is not RuleInterface', $field, $className);

                throw new BadRuleException($message);
            }
        }
    }

    /**
     * @param array $rawRules
     *
     * @return array<string, RuleInterface[]>
     */
    protected function prepareRules(array $rawRules): array
    {
        $rules = [];
        foreach ($rawRules as $attribute => $attributeRules) {
            foreach ($attributeRules as $rule) {
                if (is_string($rule)) {
                    $rule = new $rule;
                }

                $rules[$attribute][] = $rule;
            }
        }

        return $rules;
    }

    /**
     * @param $rule
     * @return bool
     */
    protected function isRule($rule): bool
    {
        $implements = class_implements($rule);
        return in_array(RuleInterface::class, $implements, true);
    }

    /**
     * @inheritDoc
     */
    public function validate(): void
    {
        foreach ($this->data as $attributeName => $attributeValue) {
            if (!isset($this->rules[$attributeName])) {
                continue;
            }
            $this->validateAttribute($attributeName, $attributeValue);
        }
    }

    /**
     * @param string $attributeName
     * @param mixed $attributeValue
     *
     * @return void
     *
     * @throws RuleExceptionInterface
     */
    protected function validateAttribute(string $attributeName, mixed $attributeValue): void
    {
        $rules = $this->rules[$attributeName];

        foreach ($rules as $rule) {
            if (!$rule->validate($attributeValue)) {
                $message = $this->messages[$attributeName] ?? $rule->message() ?? '';
                throw RuleException::make($attributeName, $message);
            }

            $this->validatedData[$attributeName] = $rule->prepare($attributeValue);
        }
    }

    /**
     * @throws BadRuleExceptionInterface
     */
    public static function make(array $rules, array $data, array $messages = []): ValidatorInterface
    {
        return new static($rules, $data, $messages);
    }

    /**
     * @return mixed[]
     * @throws RuleExceptionInterface
     */
    public function validated(): array
    {
        $this->validate();

        return $this->validatedData;
    }


}