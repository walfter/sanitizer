<?php

namespace Walfter\Sanitizer\Exceptions;

use Walfter\Sanitizer\Contracts\BadValidatorExceptionInterface;

class BadValidatorException extends \Exception implements BadValidatorExceptionInterface
{

}