<?php

namespace Walfter\Sanitizer\Exceptions;

use Walfter\Sanitizer\Contracts\BadRuleExceptionInterface;

class BadRuleException extends \Exception implements BadRuleExceptionInterface
{

}