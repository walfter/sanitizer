<?php

namespace Walfter\Sanitizer\Exceptions;

use Walfter\Sanitizer\Contracts\RuleExceptionInterface;

class RuleException extends \Exception implements RuleExceptionInterface
{
    private const VALIDATE_CODE_ERROR = 422;

    /**
     * @inheritDoc
     */
    public static function make(string $attributeName, string $template = ''): RuleExceptionInterface
    {
        $template = empty($template) ? '%s bad value' : $template;
        $message = sprintf($template, $attributeName);

        return new self($message, self::VALIDATE_CODE_ERROR);
    }
}