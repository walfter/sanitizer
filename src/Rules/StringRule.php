<?php

namespace Walfter\Sanitizer\Rules;

use Walfter\Sanitizer\Contracts\RuleInterface;

class StringRule implements RuleInterface
{

    public function validate(mixed $value): bool
    {
        return is_string($value);
    }

    public function message(): string
    {
        return '%s is not string';
    }

    public function prepare(mixed $value): string
    {
        return (string) $value;
    }
}