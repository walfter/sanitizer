<?php

namespace Walfter\Sanitizer\Rules;

use Walfter\Sanitizer\Contracts\RuleInterface;

class FloatRule implements RuleInterface
{

    public function validate(mixed $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_FLOAT) !== false;
    }

    public function message(): string
    {
        return '%s is not float value';
    }

    public function prepare(mixed $value): float
    {
        return (float) $value;
    }
}