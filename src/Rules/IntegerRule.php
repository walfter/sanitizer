<?php

namespace Walfter\Sanitizer\Rules;

use Walfter\Sanitizer\Contracts\RuleInterface;

class IntegerRule implements RuleInterface
{

    public function validate(mixed $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_INT) !== false;
    }

    public function message(): string
    {
        return '%s is not integer value';
    }

    public function prepare(mixed $value): int
    {
        return (int) $value;
    }
}