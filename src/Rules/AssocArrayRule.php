<?php

namespace Walfter\Sanitizer\Rules;

use Walfter\Sanitizer\Contracts\RuleInterface;

class AssocArrayRule implements RuleInterface
{

    public function validate(mixed $value): bool
    {
        if (!is_array($value) || array() === $value) {
            return false;
        }

        return array_keys($value) !== range(0, count($value) - 1);
    }

    public function message(): string
    {
        return '%s is not associative array';
    }

    public function prepare(mixed $value): array
    {
        return (array) $value;
    }
}