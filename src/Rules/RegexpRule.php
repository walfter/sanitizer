<?php

namespace Walfter\Sanitizer\Rules;

use Walfter\Sanitizer\Contracts\RuleInterface;

class RegexpRule implements RuleInterface
{
    private string $pattern;

    public function __construct(string $pattern)
    {
        $this->pattern = $pattern;
    }

    public function validate(mixed $value): bool
    {
        return (bool) preg_match($this->pattern, $value);
    }

    public function message(): string
    {
        return '%s is not regexp by "' . $this->pattern . '"';
    }

    public function prepare(mixed $value): mixed
    {
        return $value;
    }
}