<?php

namespace Walfter\Sanitizer\Contracts;

interface SanitizerInterface
{

    /**
     * @param array $rules
     * @param array $data
     * @param array $messages
     *
     * @return array
     *
     * @throws RuleExceptionInterface
     */
    public static function validated(array $rules, array $data, array $messages = []): array;
}