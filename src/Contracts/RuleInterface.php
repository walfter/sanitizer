<?php

namespace Walfter\Sanitizer\Contracts;

interface RuleInterface
{
    public function validate(mixed $value): bool;

    public function message(): string;

    public function prepare(mixed $value): mixed;
}