<?php

namespace Walfter\Sanitizer\Contracts;

interface RenderableExceptionInterface extends \Throwable
{
    public function render(): array;
}