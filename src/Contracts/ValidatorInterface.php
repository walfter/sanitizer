<?php

namespace Walfter\Sanitizer\Contracts;

interface ValidatorInterface
{
    /**
     * @param array $rules
     * @param array $data
     * @param array $messages
     *
     * @throws BadRuleExceptionInterface
     */
    public function __construct(array $rules, array $data = [], array $messages = []);

    public function setData(array $data): self;

    public function setMessages(array $messages): self;

    /**
     * @return void
     *
     * @throws RuleExceptionInterface
     */
    public function validate(): void;

    public static function make(array $rules, array $data, array $messages = []): self;

    /**
     * @return array
     *
     * @throws RuleExceptionInterface
     */
    public function validated(): array;
}