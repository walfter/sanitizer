<?php

namespace Walfter\Sanitizer\Contracts;

interface RuleExceptionInterface extends \Throwable
{
    /**
     * @param string $attributeName
     * @param string $template for sprintf, has one attribute, give $attributeName as arg
     *
     * @return static
     */
    public static function make(string $attributeName, string $template = ''): self;
}