<?php

use Walfter\Sanitizer\Rules\AssocArrayRule;
use Walfter\Sanitizer\Rules\FloatRule;
use Walfter\Sanitizer\Rules\IntegerRule;
use Walfter\Sanitizer\Rules\RegexpRule;
use Walfter\Sanitizer\Rules\StringRule;
use Walfter\Sanitizer\Sanitizer;

class SanitizerTest extends \PHPUnit\Framework\TestCase
{
    public function testWithoutException()
    {
        $data = Sanitizer::validated([
            'string' => [StringRule::class],
            'float' => [FloatRule::class],
            'integer' => [IntegerRule::class],
            'assoc' => [AssocArrayRule::class],
            'phone' => [new RegexpRule('/^\s?(\+\s?7|8)([- ()]*\d){10}$/')],
        ], [
            'string' => 'some string',
            'float' => '1.5',
            'integer' => '1',
            'assoc' => ['is' => 'some string'],
            'phone' => '+7 (923) 666-66-66',
        ]);

        $this->assertIsBool(is_string($data['string']));
        $this->assertIsFloat($data['float']);
        $this->assertIsInt($data['integer']);
        $this->assertIsArray($data['assoc']);
    }

    public function testWithException()
    {
        $this->expectException(\Walfter\Sanitizer\Contracts\RuleExceptionInterface::class);

        $data = Sanitizer::validated([
            'string' => [StringRule::class],
            'float' => [FloatRule::class],
            'integer' => [IntegerRule::class],
            'assoc' => [AssocArrayRule::class],
            'phone' => [new RegexpRule('/^\s?(\+\s?7|8)([- ()]*\d){10}$/')],
        ], [
            'string' => 'some string',
            'float' => '1',
            'integer' => '1.5',
            'assoc' => ['some string'],
            'phone' => '+7 (923) 666-66-66',
        ]);
    }
}